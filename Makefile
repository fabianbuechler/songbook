all: chordpro

chordpro:
	chordpro --toc --output=songbook.pdf songs/*.chopro

chordpro-watch:
	@watchmedo shell-command \
		--wait --interval 3 \
        --patterns="*.chopro;*.json" \
		--command="make chordpro" \
		--recursive \
		.

.PHONY:
	chordpro
