FROM python:latest

WORKDIR /app
ADD . /app

RUN cpan install wxchordpro

RUN pip install \
    --trusted-host pypi.python.org \
    --no-cache-dir \
    -r requirements.txt

ENTRYPOINT ["/bin/sh", "-c"]
CMD [ \
    "watchmedo", "shell-command", \
    "--wait", \
    "--interval", "3", \
    "--patterns", "*.chopro;*.json", \
    "--command", "chordpro --toc --output=songbook.pdf songs/*.chopro", \
    "--recursive", \
    "." \
]
